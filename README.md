**Project Overview:**

This Kubernetes project sets up a microservices architecture with several components:

- A MySQL database (vprodb) and its associated ClusterIP service.
- RabbitMQ (vpromq01) and its ClusterIP service.
- Memcached (vpromc) and its ClusterIP service.
- An application (vproapp) deployed as a Deployment with one replica and a LoadBalancer service for external access.
- The application, database, and RabbitMQ are all configured to use secrets (app-secret) for sensitive data like passwords.
- InitContainers ensure DNS resolution for database and memcached services before the application starts.
- The database deployment also includes an initContainer to prepare the persistent volume.

**Dependencies:**
- Proper configuration and access to a Kubernetes cluster.
- Docker images for the application (vprofile/vprofileapp:V1), database (vprofile/vprofiledb:V1), RabbitMQ, and Memcached.
- Persistent volume with ID vol-0a8109ffc382b63b2 for the database.

Let's break down each file individually to give a comprehensive Overview:

- **app-secret.yaml**
This file defines a Kubernetes Secret named app-secret with two pieces of sensitive data encoded in base64 format: db-pass and rmq-pass, which likely represent passwords for the database and RabbitMQ respectively.

- **db-CIP.yaml (ClusterIP)**
This file defines a Kubernetes Service named vprodb with a ClusterIP type, which exposes port 3306 for MySQL. It forwards traffic to pods labeled with app: vprodb.

- **mc-CIP.yaml (ClusterIP)**
This file defines a Kubernetes Service named vprocache01 with a ClusterIP type, which exposes port 11211 for memcached. It forwards traffic to pods labeled with app: vpromc.

- **mcdep.yaml (Memcached Deployment)**
This file defines a Kubernetes Deployment for a memcached pod named vpromc. It ensures one replica of the pod runs, exposing port 11211.

- **rmq-CIP.yaml (ClusterIP)**
This file defines a Kubernetes Service named vpromq01 with a ClusterIP type, which exposes port 15672 for RabbitMQ management. It forwards traffic to pods labeled with app: vpromq01.

- **rmq-dep.yaml (RabbitMQ Deployment)**
This file defines a Kubernetes Deployment for RabbitMQ named vpromq01. It ensures one replica of the RabbitMQ pod runs, exposing port 15672. It also sets the RabbitMQ username and password from the Secret app-secret.

- **vproapp-service.yaml**
This file defines a Kubernetes Service named vproapp-service of type LoadBalancer, exposing port 80 for the application. It forwards traffic to pods labeled with app: vproapp.

- **vproappdep.yaml (Application Deployment)**
This file defines a Kubernetes Deployment for the application vproapp. It ensures one replica of the application pod runs, exposing port 8080. It also includes two initContainers to ensure that DNS resolution for database and memcached services is available before the application starts.

- **vprodbdep.yaml (Database Deployment)**
This file defines a Kubernetes Deployment for the database vprodb. It ensures one replica of the database pod runs, exposing port 3306. It also sets the MySQL root password from the Secret app-secret. Additionally, it includes an initContainer to remove the lost+found directory from the persistent volume.


